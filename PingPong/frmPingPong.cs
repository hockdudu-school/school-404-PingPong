﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace PingPong
{
    public partial class FrmPingPong : Form
    {
        private int _points;
        private bool _ballControll;
        private const int BallAddNewBallPointModulo = 100;
        private const int BallSpeedAddModulo = 50;
        private const double BallSpeedAddMultiplier = 1.2;

        private readonly List<Ball> _balls = new List<Ball>();


        public FrmPingPong()
        {
            InitializeComponent();
            _balls.Add(new Ball(pnlSpiel, picSchlägerRechts));

            SetBallControll(_ballControll);
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Up ||
                keyData == Keys.Down ||
                keyData == Keys.Left ||
                keyData == Keys.Right)
            {
                return true;
            }

            return base.ProcessDialogKey(keyData);
        }

        private void TmrSpiel_Tick(object sender, EventArgs e)
        {
            var isBallToBeAdded = false;

            foreach (var ball in _balls)
            {
                ball.BallLogic(out var hitDirection, out var didBallHitRacket);

                if (hitDirection != Ball.HitDirection.Right) continue;
                if (didBallHitRacket)
                {
                    _points = _points + 10;

                    if (_ballControll)
                        ball.PutInRandomPosition();

                    if (_points % BallSpeedAddModulo == 0)
                        foreach (var ball1 in _balls)
                        {
                            ball1.GiveRandomColor();
                            ball1.SpeedX = (int) (ball1.SpeedX * BallSpeedAddMultiplier);
                        }

                    if (_points % BallAddNewBallPointModulo == 0) isBallToBeAdded = true;
                }
                else
                {
                    tmrSpiel.Stop();

                    var frmGameOver = new FrmGameOver(_points);
                    frmGameOver.ShowDialog();

                    RestartGame();
                    break;
                }
            }

            if (isBallToBeAdded) _balls.Add(new Ball(pnlSpiel, picSchlägerRechts));

            txtPunkte.Text = _points.ToString();
        }

        private void BtnStart_Click(object sender, EventArgs e) => tmrSpiel.Start();

        private void VbsSchlägerRechts_Scroll(object sender, ScrollEventArgs e)
        {
            var position = (int) ((pnlSpiel.Height - picSchlägerRechts.Height) * GetScrollRatio());
            picSchlägerRechts.Location = new Point(picSchlägerRechts.Location.X, position);
        }

        private void FrmPingPong_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.S:
                    tmrSpiel.Start();
                    break;
                case Keys.P:
                    tmrSpiel.Stop();
                    break;
                case Keys.H:
                    if (tmrSpiel.Enabled)
                        foreach (var ball in _balls)
                            ball.SpeedY = -ball.SpeedY;
                    break;
                case Keys.V:
                    if (tmrSpiel.Enabled)
                        foreach (var ball in _balls)
                            ball.SpeedX = -ball.SpeedX;
                    break;
                case Keys.Up:
                    MoveEveryBallIfGameIsRunning(Ball.MoveDirection.Up);
                    break;
                case Keys.Down:
                    MoveEveryBallIfGameIsRunning(Ball.MoveDirection.Down);
                    break;
                case Keys.Left:
                    MoveEveryBallIfGameIsRunning(Ball.MoveDirection.Left);
                    break;
                case Keys.Right:
                    MoveEveryBallIfGameIsRunning(Ball.MoveDirection.Right);
                    break;
            }
        }


        private void BtnHoch_Click(object sender, EventArgs e) =>
            MoveEveryBallIfGameIsRunning(Ball.MoveDirection.Up);

        private void BtnRunter_Click(object sender, EventArgs e) =>
            MoveEveryBallIfGameIsRunning(Ball.MoveDirection.Down);

        private void BtnLinks_Click(object sender, EventArgs e) =>
            MoveEveryBallIfGameIsRunning(Ball.MoveDirection.Left);

        private void BtnRechts_Click(object sender, EventArgs e) =>
            MoveEveryBallIfGameIsRunning(Ball.MoveDirection.Right);

        private void RbdSchläger_CheckedChanged(object sender, EventArgs e) =>
            SetBallControll(!((RadioButton) sender).Checked);

        private void RdbBall_CheckedChanged(object sender, EventArgs e) =>
            SetBallControll(((RadioButton) sender).Checked);


        private void MoveEveryBallIfGameIsRunning(Ball.MoveDirection moveDirection)
        {
            if (!tmrSpiel.Enabled) return;

            foreach (var ball in _balls)
                ball.Move(moveDirection);
        }


        private void RestartGame()
        {
            _points = 0;
            txtPunkte.Text = _points.ToString();

            _balls.ForEach(ball => ball.Dispose());
            _balls.Clear();
            _balls.Add(new Ball(pnlSpiel, picSchlägerRechts));
        }
        
        private double GetScrollRatio()
        {
            var scrollPosition = vbsSchlägerRechts.Value;
            var normalizedScrollHeight = vbsSchlägerRechts.Maximum - vbsSchlägerRechts.Minimum;
            var possibleScrollHeight = normalizedScrollHeight - vbsSchlägerRechts.LargeChange;

            var ratio = (double) scrollPosition / possibleScrollHeight;

            if (ratio > 1) ratio = 1;

            return ratio;
        }

        private void SetBallControll(bool isBallControll)
        {
            _ballControll = isBallControll;
            if (isBallControll)
            {
                vbsSchlägerRechts.Enabled = false;
                btnHoch.Enabled = true;
                btnRunter.Enabled = true;
                btnLinks.Enabled = true;
                btnRechts.Enabled = true;
            }
            else
            {
                vbsSchlägerRechts.Enabled = true;
                btnHoch.Enabled = false;
                btnRunter.Enabled = false;
                btnLinks.Enabled = false;
                btnRechts.Enabled = false;
            }
        }
    }
}