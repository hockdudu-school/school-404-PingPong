﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace PingPong
{
    public class Highscore
    {
        public string User { get; set; }
        public int Score { get; set; }
        public DateTime Date { get; set; }


        private static readonly string HighscorePath = Application.StartupPath + "/highscore.xml";

        public Highscore(string user, int score): this(user, score, DateTime.Today){}

        public Highscore(string user, int score, DateTime date)
        {
            User = user;
            Score = score;
            Date = date;
        }

        public Highscore() {}

        public static IEnumerable<Highscore> ReadHighscores()
        {
            try
            {
                using (var reader = new StreamReader(HighscorePath))
                {
                    var serializer = new XmlSerializer(typeof(List<Highscore>));
                    return (List<Highscore>) serializer.Deserialize(reader);
                }
            }
            catch (FileNotFoundException)
            {
                return new List<Highscore>();
            }
        }

        public static void WriteHighscores(List<Highscore> highscores)
        {
            var serializer = new XmlSerializer(typeof(List<Highscore>));
            using (var writer = new StreamWriter(HighscorePath))
            {
                serializer.Serialize(writer, highscores);
            }
        }


        public static IEnumerable<Highscore> GetSortedHighscores() => ReadHighscores().OrderByDescending(o => o.Score);

        public static void AppendHighscoreToFile(Highscore highscore)
        {
            var previousHighscore = ReadHighscores().ToList();
            previousHighscore.Add(highscore);

            var sortedList = previousHighscore.OrderByDescending(o => o.Score).ToList();

            WriteHighscores(sortedList);
        }
    }
}
