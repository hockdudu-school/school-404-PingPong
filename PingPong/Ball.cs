﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PingPong
{
    internal class Ball
    {
        private static readonly Random Random = new Random();

        private readonly Control _parent;
        private readonly Control _racket;
        private readonly Control _self;

        public int SpeedX = 5;
        public int SpeedY = 2;

        public static int ArrowSpeed = 25;

        private static readonly Color[] Colors =
        {
            Color.BlueViolet,
            Color.DeepPink,
            Color.DeepSkyBlue,
            Color.Gold,
            Color.Lime,
            Color.MidnightBlue,
            Color.OrangeRed,
            Color.Tomato,
            Color.Turquoise
        };

        public enum MoveDirection
        {
            Up,
            Down,
            Left,
            Right
        }

        public enum HitDirection
        {
            None,
            Top,
            Bottom,
            Right,
            Left,
            TopLeft,
            TopRight,
            BottomLeft,
            BottomRight
        }

        public Ball(Control parent, Control racket)
        {
            _parent = parent;
            _racket = racket;

            _self = new PictureBox
            {
                BackColor = Colors[Random.Next(Colors.Length)],
                Size = new Size(25, 25)
            };
            _self.Location = GetSafeRandomBallPosition();
            _parent.Controls.Add(_self);
        }

        public void BallLogic(out HitDirection hitDirection, out bool didBallHitRacket)
        {
            var currentPosition = _self.Location;
            var wantedPosition = new Point(currentPosition.X + SpeedX, currentPosition.Y + SpeedY);

            var allowedPosition = GetAllowedBallPosition(wantedPosition, out hitDirection, out didBallHitRacket);

            if (allowedPosition.X != wantedPosition.X) SpeedX = -SpeedX;
            if (allowedPosition.Y != wantedPosition.Y) SpeedY = -SpeedY;

            _self.Location = allowedPosition;
        }

        public void Move(MoveDirection moveDirection)
        {
            var moveSize = new Size(0, 0);

            switch (moveDirection)
            {
                case MoveDirection.Up:
                    moveSize.Height -= ArrowSpeed;
                    break;
                case MoveDirection.Right:
                    moveSize.Width += ArrowSpeed;
                    break;
                case MoveDirection.Down:
                    moveSize.Height += ArrowSpeed;
                    break;
                case MoveDirection.Left:
                    moveSize.Width -= ArrowSpeed;
                    break;
                default:
                    throw new NotImplementedException();
            }

            var movePosition = Point.Add(_self.Location, moveSize);

            _self.Location = GetAllowedBallPosition(movePosition, out _, out _);
        }

        public void PutInRandomPosition()
        {
            SpeedX = Math.Abs(SpeedX);
            _self.Location = GetSafeRandomBallPosition();
            if (new Random().NextDouble() >= 0.5)
                SpeedY = -SpeedY;
        }

        public void GiveRandomColor() => _self.BackColor = Colors[Random.Next(Colors.Length)];

        public void Dispose() => _self.Dispose();

        private Point GetAllowedBallPosition(Point wantedPosition, out HitDirection hitHitDirection,
            out bool didBallHitRacket)
        {
            var allowedPosition = wantedPosition;
            hitHitDirection = HitDirection.None;
            didBallHitRacket = false;

            if (wantedPosition.X < 0)
            {
                allowedPosition.X = 0;
                hitHitDirection = HitDirection.Left;
            }
            else if (WouldBallOverlapWithRacketIfItWereAtThisLocation(wantedPosition))
            {
                allowedPosition.X = _racket.Location.X - _self.Width;
                didBallHitRacket = true;
                hitHitDirection = HitDirection.Right;
            }
            else if (wantedPosition.X + _self.Width > _parent.Width)
            {
                allowedPosition.X = _parent.Width - _self.Width;
                hitHitDirection = HitDirection.Right;
            }


            if (wantedPosition.Y < 0)
            {
                allowedPosition.Y = 0;

                switch (hitHitDirection)
                {
                    case HitDirection.Left:
                        hitHitDirection = HitDirection.TopLeft;
                        break;
                    case HitDirection.Right:
                        hitHitDirection = HitDirection.TopRight;
                        break;
                    default:
                        hitHitDirection = HitDirection.Top;
                        break;
                }
            }
            else if (wantedPosition.Y + _self.Height > _parent.Height)
            {
                allowedPosition.Y = _parent.Height - _self.Height;

                switch (hitHitDirection)
                {
                    case HitDirection.Left:
                        hitHitDirection = HitDirection.BottomLeft;
                        break;
                    case HitDirection.Right:
                        hitHitDirection = HitDirection.BottomRight;
                        break;
                    default:
                        hitHitDirection = HitDirection.Bottom;
                        break;
                }
            }

            return allowedPosition;
        }

        private bool WouldBallOverlapWithRacketIfItWereAtThisLocation(Point ballLocation)
        {
            var racketRectangle = new Rectangle(_racket.Location, _racket.Size);
            var ballRectangle = new Rectangle(ballLocation, _self.Size);

            return ballRectangle.IntersectsWith(racketRectangle);
        }

        private Point GetSafeRandomBallPosition()
        {
            var minX = _self.Width;
            var maxX = (int) ((_parent.Width - _self.Width) * 0.5);

            var minY = _self.Height;
            var maxY = (_parent.Height - _self.Height) - _self.Height;

            var x = Random.Next(minX, maxX);
            var y = Random.Next(minY, maxY);

            return new Point(x, y);
        }
    }
}