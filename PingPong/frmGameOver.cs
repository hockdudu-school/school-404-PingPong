﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace PingPong
{
    public partial class FrmGameOver : Form
    {

        private readonly int _score;
        private Highscore _userHighscore;

        public FrmGameOver(int score)
        {
            InitializeComponent();

            _score = score;
            lblPunkte.Text = score.ToString();


            SetHighscoresLabelText(Highscore.GetSortedHighscores());
        }


        private void SetHighscoresLabelText(IEnumerable<Highscore> highscores)
        {
            var stringBuilder = new StringBuilder();

            foreach (var highscore in highscores)
            {
                var scoreText = $"{highscore.Score,-5} {highscore.User,-20} {highscore.Date.ToShortDateString()}";
                stringBuilder.AppendLine(scoreText);
            }

            lblErgebnisse.Text = stringBuilder.ToString();
        }

        private void BtnEintragen_Click(object sender, EventArgs e)
        {
            if (_userHighscore != null) return;
        
            var user = txtName.Text;

            if (string.IsNullOrWhiteSpace(user)) return;


            _userHighscore = new Highscore(user, _score);
            Highscore.AppendHighscoreToFile(_userHighscore);
            SetHighscoresLabelText(Highscore.GetSortedHighscores());

            btnEintragen.Enabled = false;
            txtName.Enabled = false;
        }

        private void BtnSchliessen_Click(object sender, EventArgs e)
        {
            Close();
        }
        
        
    }
}
