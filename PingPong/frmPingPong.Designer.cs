﻿namespace PingPong
{
    partial class FrmPingPong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlSpiel = new System.Windows.Forms.Panel();
            this.picSchlägerRechts = new System.Windows.Forms.PictureBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.tmrSpiel = new System.Windows.Forms.Timer(this.components);
            this.vbsSchlägerRechts = new System.Windows.Forms.VScrollBar();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPunkte = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnRechts = new System.Windows.Forms.Button();
            this.btnLinks = new System.Windows.Forms.Button();
            this.btnRunter = new System.Windows.Forms.Button();
            this.btnHoch = new System.Windows.Forms.Button();
            this.grpSteuerung = new System.Windows.Forms.GroupBox();
            this.rbdSchläger = new System.Windows.Forms.RadioButton();
            this.rdbBall = new System.Windows.Forms.RadioButton();
            this.pnlSpiel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picSchlägerRechts)).BeginInit();
            this.grpSteuerung.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSpiel
            // 
            this.pnlSpiel.BackColor = System.Drawing.Color.SeaGreen;
            this.pnlSpiel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSpiel.Controls.Add(this.picSchlägerRechts);
            this.pnlSpiel.Location = new System.Drawing.Point(12, 12);
            this.pnlSpiel.Name = "pnlSpiel";
            this.pnlSpiel.Size = new System.Drawing.Size(644, 316);
            this.pnlSpiel.TabIndex = 0;
            // 
            // picSchlägerRechts
            // 
            this.picSchlägerRechts.BackColor = System.Drawing.Color.Black;
            this.picSchlägerRechts.Location = new System.Drawing.Point(635, 138);
            this.picSchlägerRechts.Name = "picSchlägerRechts";
            this.picSchlägerRechts.Size = new System.Drawing.Size(4, 40);
            this.picSchlägerRechts.TabIndex = 1;
            this.picSchlägerRechts.TabStop = false;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(13, 335);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Spiel Starten";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // tmrSpiel
            // 
            this.tmrSpiel.Interval = 16;
            this.tmrSpiel.Tick += new System.EventHandler(this.TmrSpiel_Tick);
            // 
            // vbsSchlägerRechts
            // 
            this.vbsSchlägerRechts.Location = new System.Drawing.Point(659, 12);
            this.vbsSchlägerRechts.Name = "vbsSchlägerRechts";
            this.vbsSchlägerRechts.Size = new System.Drawing.Size(17, 316);
            this.vbsSchlägerRechts.TabIndex = 2;
            this.vbsSchlägerRechts.Value = 45;
            this.vbsSchlägerRechts.Scroll += new System.Windows.Forms.ScrollEventHandler(this.VbsSchlägerRechts_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(136, 340);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Punkte:";
            // 
            // txtPunkte
            // 
            this.txtPunkte.Location = new System.Drawing.Point(187, 337);
            this.txtPunkte.Name = "txtPunkte";
            this.txtPunkte.Size = new System.Drawing.Size(100, 20);
            this.txtPunkte.TabIndex = 4;
            this.txtPunkte.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(136, 362);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(284, 104);
            this.label2.TabIndex = 9;
            this.label2.Text = "$ man ping-pong\r\n\r\nOh wait, you\'re using \"Windows\"\r\n\r\n\'man\' is not recognized as " +
    "an internal or external command,\r\noperable program or batch file.\r\n\r\nSUCK THAT!";
            // 
            // btnRechts
            // 
            this.btnRechts.BackColor = System.Drawing.Color.White;
            this.btnRechts.BackgroundImage = global::PingPong.Properties.Resources.rechts;
            this.btnRechts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRechts.Location = new System.Drawing.Point(766, 158);
            this.btnRechts.Name = "btnRechts";
            this.btnRechts.Size = new System.Drawing.Size(23, 23);
            this.btnRechts.TabIndex = 8;
            this.btnRechts.UseVisualStyleBackColor = false;
            this.btnRechts.Click += new System.EventHandler(this.BtnRechts_Click);
            // 
            // btnLinks
            // 
            this.btnLinks.BackColor = System.Drawing.Color.White;
            this.btnLinks.BackgroundImage = global::PingPong.Properties.Resources.links;
            this.btnLinks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLinks.Location = new System.Drawing.Point(708, 158);
            this.btnLinks.Name = "btnLinks";
            this.btnLinks.Size = new System.Drawing.Size(23, 23);
            this.btnLinks.TabIndex = 7;
            this.btnLinks.UseVisualStyleBackColor = false;
            this.btnLinks.Click += new System.EventHandler(this.BtnLinks_Click);
            // 
            // btnRunter
            // 
            this.btnRunter.BackColor = System.Drawing.Color.White;
            this.btnRunter.BackgroundImage = global::PingPong.Properties.Resources.runter;
            this.btnRunter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRunter.Location = new System.Drawing.Point(737, 187);
            this.btnRunter.Name = "btnRunter";
            this.btnRunter.Size = new System.Drawing.Size(23, 23);
            this.btnRunter.TabIndex = 6;
            this.btnRunter.UseVisualStyleBackColor = false;
            this.btnRunter.Click += new System.EventHandler(this.BtnRunter_Click);
            // 
            // btnHoch
            // 
            this.btnHoch.BackColor = System.Drawing.Color.White;
            this.btnHoch.BackgroundImage = global::PingPong.Properties.Resources.hoch;
            this.btnHoch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHoch.Location = new System.Drawing.Point(737, 129);
            this.btnHoch.Name = "btnHoch";
            this.btnHoch.Size = new System.Drawing.Size(23, 23);
            this.btnHoch.TabIndex = 5;
            this.btnHoch.UseVisualStyleBackColor = false;
            this.btnHoch.Click += new System.EventHandler(this.BtnHoch_Click);
            // 
            // grpSteuerung
            // 
            this.grpSteuerung.Controls.Add(this.rbdSchläger);
            this.grpSteuerung.Controls.Add(this.rdbBall);
            this.grpSteuerung.Location = new System.Drawing.Point(531, 340);
            this.grpSteuerung.Name = "grpSteuerung";
            this.grpSteuerung.Size = new System.Drawing.Size(145, 72);
            this.grpSteuerung.TabIndex = 10;
            this.grpSteuerung.TabStop = false;
            this.grpSteuerung.Text = "Wahl der Steuerung";
            // 
            // rbdSchläger
            // 
            this.rbdSchläger.AutoSize = true;
            this.rbdSchläger.Checked = true;
            this.rbdSchläger.Location = new System.Drawing.Point(7, 44);
            this.rbdSchläger.Name = "rbdSchläger";
            this.rbdSchläger.Size = new System.Drawing.Size(114, 17);
            this.rbdSchläger.TabIndex = 1;
            this.rbdSchläger.TabStop = true;
            this.rbdSchläger.Text = "Schlägersteuerung";
            this.rbdSchläger.UseVisualStyleBackColor = true;
            this.rbdSchläger.CheckedChanged += new System.EventHandler(this.RbdSchläger_CheckedChanged);
            // 
            // rdbBall
            // 
            this.rdbBall.AutoSize = true;
            this.rdbBall.Location = new System.Drawing.Point(7, 20);
            this.rdbBall.Name = "rdbBall";
            this.rdbBall.Size = new System.Drawing.Size(89, 17);
            this.rdbBall.TabIndex = 0;
            this.rdbBall.Text = "Ballsteuerung";
            this.rdbBall.UseVisualStyleBackColor = true;
            this.rdbBall.CheckedChanged += new System.EventHandler(this.RdbBall_CheckedChanged);
            // 
            // frmPingPong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 482);
            this.Controls.Add(this.grpSteuerung);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnRechts);
            this.Controls.Add(this.btnLinks);
            this.Controls.Add(this.btnRunter);
            this.Controls.Add(this.btnHoch);
            this.Controls.Add(this.txtPunkte);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.vbsSchlägerRechts);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.pnlSpiel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "FrmPingPong";
            this.Text = "Ping-Pong Spiel";
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FrmPingPong_KeyUp);
            this.pnlSpiel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picSchlägerRechts)).EndInit();
            this.grpSteuerung.ResumeLayout(false);
            this.grpSteuerung.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlSpiel;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Timer tmrSpiel;
        private System.Windows.Forms.VScrollBar vbsSchlägerRechts;
        private System.Windows.Forms.PictureBox picSchlägerRechts;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPunkte;
        private System.Windows.Forms.Button btnHoch;
        private System.Windows.Forms.Button btnRunter;
        private System.Windows.Forms.Button btnLinks;
        private System.Windows.Forms.Button btnRechts;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox grpSteuerung;
        private System.Windows.Forms.RadioButton rbdSchläger;
        private System.Windows.Forms.RadioButton rdbBall;
    }
}

